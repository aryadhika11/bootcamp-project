// soal1
let luasPersegiPanjang = (a, b) => {
    console.log(a * b);
}
const kelilingPersegiPanjang = (a, b) => {
    console.log(2 * (a + b));
}
let pilihan = 2;
switch (pilihan) {
    case 1:
        luasPersegiPanjang(2, 4);
        break;
    case 2:
        kelilingPersegiPanjang(3, 6);
        break;
    default:
        console.log("pilihan tidak tersedia");
}

// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
// soal2
const gabungNama = (namaDepan, namaBelakang) => {
    return {
        namaDepan,
        namaBelakang,
        namaLengkap() {
            console.log(namaDepan + " " + namaBelakang);
        }
    }
}
gabungNama("william", "Imoh").namaLengkap();

// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
// soal3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
// soal4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east];
console.log(combined);

// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// soal5
const planet = "earth"
const view = "glass"
console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`);