<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginSignupController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CheckTokenController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\SocialiteController;
use App\Models\Campaign;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('/register', [LoginSignupController::class, 'register']);
    Route::post('/otp-verify', [LoginSignupController::class, 'verifyOTPCodes']);
    Route::post('/regenerate', [LoginSignupController::class, 'regenerateOTPCodes']);
    Route::post('/updatePassword', [LoginSignupController::class, 'updatePassword']);
    Route::post('/login', [LoginSignupController::class, 'login']);
    Route::post('/logout', [LogoutController::class, 'logout']);
    Route::post('check-token', [CheckTokenController::class]);
    Route::post('/getNewPassword', [SocialiteController::class, 'getNewPassword']);

    Route::get('/social/{provider}', [SocialiteController::class, 'redirectToProvider']);
    Route::get('/social/{provider}/callback', [SocialiteController::class, 'handleProviderCallback']);
});

//Profile Routes
Route::group(['middleware' => 'guest'], function () {
    Route::prefix('profile')->group(function () {
        Route::get('getProfile', [DashboardController::class, 'getProfile']);
        Route::post('/updateProfile', [DashboardController::class, 'updateProfile']);
    });
});

//Campaigns Routes
Route::prefix('campaign')->group(function () {
    Route::get('random/{count}', [CampaignController::class, 'random']);
    Route::post('store', [CampaignController::class, 'store']);
    Route::get('/', [CampaignController::class, 'index']);
    Route::get('/{id}', [CampaignController::class, 'detail']);
    Route::get('/search/{keyword}', [CampaignController::class, 'search']);
    Route::get('/delete/{id}', [CampaignController::class, 'delete']);
    Route::get('/edit/{id}', [CampaignController::class, 'edit']);
});

//Blogs Routes
Route::prefix('blog')->group(function () {
    Route::get('random/{count}', [BlogController::class, 'random']);
    Route::post('store', [BlogController::class, 'store']);
});
