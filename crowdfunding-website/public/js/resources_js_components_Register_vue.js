"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_Register_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Register.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Register.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'register',
  data: function data() {
    return {
      valid: true,
      username: '',
      kondisiSelect: false,
      kondisi: true,
      usernameRules: [function (v) {
        return !!v || 'E-mail is required';
      }, function (v) {
        return v && v.length <= 10 || 'Max 10 characters';
      }],
      email: '',
      emailRules: [function (v) {
        return !!v || 'E-mail is required';
      }, function (v) {
        return /([a-zA-Z0-9_]{1,})(@)([a-zA-Z0-9_]{2,}).([a-zA-Z0-9_]{2,})+/.test(v) || 'E-mail must be valid';
      }],
      showPassword: false,
      password: '',
      passwordRules: [function (v) {
        return !!v || 'Password is required.';
      }, function (v) {
        return v && v.length >= 8 || 'Min 8 characters';
      }],
      passwordOwnerRules: [function (v) {
        return !!v || 'Password is required.';
      }, function (v) {
        return v && v.length >= 6 || 'Min 6 characters';
      }],
      fullname: '',
      fullnameRules: [function (v) {
        return !!v || 'Fullname is required.';
      }],
      imageUrl: '',
      imageFile: null,
      imageName: '',
      imageRules: [function (v) {
        return !!v || 'Image is required';
      }],
      items: [{
        text: "User",
        value: "user"
      }, {
        text: "Admin",
        value: "admin"
      }],
      itemsRules: [function (v) {
        return !!v || 'Item is required.';
      }],
      e1: 1,
      select: '',
      pilih: '',
      kondisiVerify: false,
      passwordOwner: '',
      checkbox: '',
      kondisiBox: false,
      otp: ''
    };
  },
  computed: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_0__.mapGetters)({
    user: 'auth/user'
  })), {}, {
    buttonCheckValidation: function buttonCheckValidation() {
      return this.select == 'admin', this.kondisi == false;
    }
  }),
  methods: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_0__.mapActions)({
    setAlert: 'alert/set',
    setAuth: 'auth/set'
  })), {}, {
    verifyPasswordOwner: function verifyPasswordOwner() {
      var password = 'sayaowner';

      if (password === this.passwordOwner) {
        this.kondisiSelect = false;
        this.kondisi = true;
        this.setAlert({
          status: true,
          color: 'success',
          text: 'Hallo, Selamat datang owner!'
        });
      } else {
        this.setAlert({
          status: true,
          color: 'error',
          text: 'Password anda tidak sama'
        });
      }
    },
    clear: function clear() {
      this.$data.username = '';
      this.$data.email = '';
      this.$data.password = '';
      this.$data.fullname = '';
      this.$data.select = '';
    },
    submit: function submit(e1p) {
      var _this = this;

      if (this.checkbox != null) {
        if (this.$refs.form.validate()) {
          var formData = new FormData();
          console.log(this.form);
          formData.append('image_file', this.imageFile);
          console.log(this.imageFile);
          var fd = {
            'username': this.username,
            'email': this.email,
            'password': this.password,
            'image': formData,
            'name': this.fullname,
            'role_id': this.select
          };
          axios.post('/api/auth/register', fd).then(function (response) {
            _this.e1 = e1p;
          })["catch"](function (error) {
            console.log(error);
            var responses = error.response;

            _this.setAlert({
              status: true,
              text: responses.data,
              color: 'error'
            });
          });
        }
      } else {
        this.setAlert({
          status: true,
          text: 'Please to checklist term and conditions',
          color: 'error'
        });
      }
    },
    onFilePicked: function onFilePicked(files) {
      var _this2 = this;

      if (files !== undefined) {
        this.imageName = files.name;

        if (this.imageName.lastIndexOf('.') <= 0) {
          return;
        }

        var fr = new FileReader();
        fr.readAsDataURL(files);
        fr.addEventListener('load', function () {
          _this2.imageUrl = fr.result;
          _this2.imageFile = files;
        });
      } else {
        this.imageName = '';
        this.imageFile = '';
        this.imageUrl = '';
      }
    },
    verifyOTPCodes: function verifyOTPCodes(e2p) {
      var _this3 = this;

      var otpCodes = {
        'otp': this.otp
      };
      axios.post('/api/auth/otp-verify', otpCodes).then(function (response) {
        if (response.data.status_code == '00') {
          _this3.e1 = e2p;
        } else {
          if (response.data.message == 'Kode OTP anda sudah tidak berlaku. Segera request OTP kembali!') {
            _this3.setAlert({
              status: true,
              text: response.data.message,
              color: 'error'
            });

            _this3.kondisi = false;
          } else {
            _this3.setAlert({
              status: true,
              text: response.data.message,
              color: 'error'
            });
          }
        }
      })["catch"](function (error) {
        var responses = error.response;

        _this3.setAlert({
          status: true,
          text: responses.data.message,
          color: 'error'
        });
      });
    },
    getNewOTPCodes: function getNewOTPCodes() {
      var _this4 = this;

      var email = {
        'email': this.email
      };
      axios.post('/api/auth/regenerate', email).then(function (response) {
        _this4.setAlert({
          status: true,
          text: response.data.message,
          color: 'success'
        });

        _this4.kondisi = false;
      })["catch"](function (error) {
        var responses = error.response;

        _this4.setAlert({
          status: true,
          text: responses.data.message,
          color: 'error'
        });
      });
    },
    succesRegister: function succesRegister() {
      this.setAlert({
        status: true,
        text: "Please login, to get your new account",
        color: 'success'
      });
      this.close();
    },
    close: function close() {
      this.$emit('closed', false);
    }
  })
});

/***/ }),

/***/ "./resources/js/components/Register.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/Register.vue ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Register.vue?vue&type=template&id=97358ae4& */ "./resources/js/components/Register.vue?vue&type=template&id=97358ae4&");
/* harmony import */ var _Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Register.vue?vue&type=script&lang=js& */ "./resources/js/components/Register.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__.render,
  _Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Register.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Register.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/Register.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Register.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Register.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Register.vue?vue&type=template&id=97358ae4&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/Register.vue?vue&type=template&id=97358ae4& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Register.vue?vue&type=template&id=97358ae4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Register.vue?vue&type=template&id=97358ae4&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Register.vue?vue&type=template&id=97358ae4&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Register.vue?vue&type=template&id=97358ae4& ***!
  \********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    [
      _c(
        "v-toolbar",
        { attrs: { dark: "", color: "success" } },
        [
          _c(
            "v-btn",
            {
              attrs: { icon: "", dark: "" },
              nativeOn: {
                click: function ($event) {
                  return _vm.close.apply(null, arguments)
                },
              },
            },
            [_c("v-icon", [_vm._v("mdi-close")])],
            1
          ),
          _vm._v(" "),
          _c("v-toolbar-title", [_vm._v("Register")]),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-stepper",
        {
          model: {
            value: _vm.e1,
            callback: function ($$v) {
              _vm.e1 = $$v
            },
            expression: "e1",
          },
        },
        [
          _c(
            "v-stepper-header",
            [
              _c(
                "v-stepper-step",
                { attrs: { complete: _vm.e1 > 1, step: "1" } },
                [_vm._v("\n                  Fill Your Bio\n              ")]
              ),
              _vm._v(" "),
              _c("v-divider"),
              _vm._v(" "),
              _c(
                "v-stepper-step",
                { attrs: { complete: _vm.e1 > 2, step: "2" } },
                [_vm._v("\n                  Verify OTP Codes\n              ")]
              ),
              _vm._v(" "),
              _c("v-divider"),
              _vm._v(" "),
              _c("v-stepper-step", { attrs: { step: "3" } }, [
                _vm._v("\n                  Result\n              "),
              ]),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-stepper-items",
            [
              _c(
                "v-stepper-content",
                { attrs: { step: "1" } },
                [
                  _c(
                    "v-container",
                    { attrs: { fluid: "" } },
                    [
                      _c("div"),
                      _vm._v(" "),
                      _c(
                        "v-form",
                        {
                          ref: "form",
                          attrs: {
                            "lazy-validation": "",
                            enctype: "multipart/form-data",
                          },
                          model: {
                            value: _vm.valid,
                            callback: function ($$v) {
                              _vm.valid = $$v
                            },
                            expression: "valid",
                          },
                        },
                        [
                          _c("v-text-field", {
                            attrs: {
                              rules: _vm.usernameRules,
                              label: "Username",
                              counter: 10,
                              required: "",
                              "append-icon": "mdi-account",
                            },
                            model: {
                              value: _vm.username,
                              callback: function ($$v) {
                                _vm.username = $$v
                              },
                              expression: "username",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              rules: _vm.emailRules,
                              label: "E-mail",
                              required: "",
                              "append-icon": "mdi-email",
                            },
                            model: {
                              value: _vm.email,
                              callback: function ($$v) {
                                _vm.email = $$v
                              },
                              expression: "email",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              "append-icon": _vm.showPassword
                                ? "mdi-eye"
                                : "mdi-eye-off",
                              rules: _vm.passwordRules,
                              type: _vm.showPassword ? "text" : "password",
                              label: "Password",
                              hint: "At least 8 characters",
                              counter: "",
                            },
                            on: {
                              "click:append": function ($event) {
                                _vm.showPassword = !_vm.showPassword
                              },
                            },
                            model: {
                              value: _vm.password,
                              callback: function ($$v) {
                                _vm.password = $$v
                              },
                              expression: "password",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              rules: _vm.fullnameRules,
                              counter: 10,
                              label: "Fullname",
                              required: "",
                              "append-icon": "mdi-account-box-outline",
                            },
                            model: {
                              value: _vm.fullname,
                              callback: function ($$v) {
                                _vm.fullname = $$v
                              },
                              expression: "fullname",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-file-input", {
                            ref: "file",
                            attrs: {
                              "show-size": "",
                              counter: "",
                              rules: _vm.imageRules,
                              type: "file",
                              accept: "image/png, image/jpeg, image/jpg",
                              placeholder: "Pick an avatar",
                              "prepend-icon": "mdi-camera",
                              label: "Choose Your Photo Profile",
                            },
                            on: { change: _vm.onFilePicked },
                          }),
                          _vm._v(" "),
                          _c("center", [
                            _vm.imageUrl
                              ? _c("img", {
                                  attrs: {
                                    src: _vm.imageUrl,
                                    width: "30%",
                                    height: "30%",
                                    align: "center",
                                  },
                                })
                              : _vm._e(),
                          ]),
                          _vm._v(" "),
                          _vm.select == "admin"
                            ? _c("div", [
                                _vm._v(_vm._s((this.kondisiSelect = true))),
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              rules: _vm.itemsRules,
                              items: _vm.items,
                              label: "What's your role?",
                              required: "",
                            },
                            model: {
                              value: _vm.select,
                              callback: function ($$v) {
                                _vm.select = $$v
                              },
                              expression: "select",
                            },
                          }),
                          _vm._v(" "),
                          _vm.select == "admin"
                            ? _c(
                                "div",
                                { staticClass: "text-center" },
                                [
                                  _c(
                                    "v-dialog",
                                    {
                                      attrs: { width: "500" },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "activator",
                                            fn: function (ref) {
                                              var on = ref.on
                                              var attrs = ref.attrs
                                              return [
                                                _c(
                                                  "v-btn",
                                                  _vm._g(
                                                    _vm._b(
                                                      {
                                                        attrs: {
                                                          color:
                                                            "red lighten-2",
                                                          dark: "",
                                                        },
                                                      },
                                                      "v-btn",
                                                      attrs,
                                                      false
                                                    ),
                                                    on
                                                  ),
                                                  [
                                                    _vm._v(
                                                      "\n                                      Verify as Owner\n                                      "
                                                    ),
                                                  ]
                                                ),
                                              ]
                                            },
                                          },
                                        ],
                                        null,
                                        false,
                                        2262545395
                                      ),
                                    },
                                    [
                                      _vm._v(" "),
                                      _c(
                                        "v-card",
                                        [
                                          _c(
                                            "v-card-title",
                                            {
                                              staticClass:
                                                "text-h5 grey lighten-2",
                                            },
                                            [
                                              _vm._v(
                                                "\n                                      Please input your password\n                                      "
                                              ),
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("v-card-text", [
                                            _c("br"),
                                            _vm._v(
                                              "\n                                      We need to know if you have owner privillage to make someone as an admin.\n                                      "
                                            ),
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "v-card-text",
                                            {
                                              attrs: {
                                                cols: "12",
                                                sm: "6",
                                                md: "4",
                                              },
                                            },
                                            [
                                              _c("v-text-field", {
                                                attrs: {
                                                  "append-icon":
                                                    _vm.showPassword
                                                      ? "mdi-eye"
                                                      : "mdi-eye-off",
                                                  rules: _vm.passwordOwnerRules,
                                                  type: _vm.showPassword
                                                    ? "text"
                                                    : "password",
                                                  label: "Password",
                                                  hint: "At least 6 characters",
                                                  counter: "",
                                                },
                                                on: {
                                                  "click:append": function (
                                                    $event
                                                  ) {
                                                    _vm.showPassword =
                                                      !_vm.showPassword
                                                  },
                                                },
                                                model: {
                                                  value: _vm.passwordOwner,
                                                  callback: function ($$v) {
                                                    _vm.passwordOwner = $$v
                                                  },
                                                  expression: "passwordOwner",
                                                },
                                              }),
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-card-actions",
                                            [
                                              _c("v-spacer"),
                                              _vm._v(" "),
                                              _c(
                                                "v-btn",
                                                {
                                                  attrs: {
                                                    color: "primary",
                                                    text: "",
                                                  },
                                                  on: {
                                                    click: function ($event) {
                                                      return _vm.verifyPasswordOwner()
                                                    },
                                                  },
                                                },
                                                [
                                                  _vm._v(
                                                    "\n                                              Verify\n                                          "
                                                  ),
                                                ]
                                              ),
                                            ],
                                            1
                                          ),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.select != "admin"
                            ? _c("v-checkbox", {
                                attrs: {
                                  label:
                                    "I agree term and conditions with this application",
                                  required: "",
                                },
                                model: {
                                  value: _vm.kondisi,
                                  callback: function ($$v) {
                                    _vm.kondisi = $$v
                                  },
                                  expression: "kondisi",
                                },
                              })
                            : _vm._e(),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: {
                        color: "primary",
                        disabled:
                          this.kondisiSelect == true || this.kondisi == false,
                      },
                      on: {
                        click: function ($event) {
                          return _vm.submit((_vm.e1p = 2))
                        },
                      },
                    },
                    [
                      _vm._v(
                        "\n                      Continue\n                  "
                      ),
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { text: "" },
                      on: {
                        click: function ($event) {
                          return _vm.clear()
                        },
                      },
                    },
                    [
                      _vm._v(
                        "\n                      Clear\n                  "
                      ),
                    ]
                  ),
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-stepper-content",
                { attrs: { step: "2" } },
                [
                  _c("br"),
                  _vm._v(" "),
                  _c(
                    "v-container",
                    {
                      staticClass: "ma-auto position-relative",
                      attrs: { fluid: "" },
                    },
                    [
                      _c(
                        "v-row",
                        [
                          _c("v-col", { attrs: { cols: "9" } }, [
                            _c(
                              "div",
                              {
                                staticClass: "ma-auto",
                                staticStyle: { "max-width": "300px" },
                              },
                              [
                                _c("br"),
                                _vm._v(" "),
                                _c(
                                  "v-otp-input",
                                  {
                                    attrs: { color: "primary" },
                                    model: {
                                      value: _vm.otp,
                                      callback: function ($$v) {
                                        _vm.otp = $$v
                                      },
                                      expression: "otp",
                                    },
                                  },
                                  [
                                    _vm.otp.length >= 6
                                      ? _c("div", [
                                          _vm._v(
                                            " " +
                                              _vm._s((_vm.kondisiVerify = true))
                                          ),
                                        ])
                                      : _vm._e(),
                                  ]
                                ),
                              ],
                              1
                            ),
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { staticClass: "my-3", attrs: { cols: "2" } },
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    block: "",
                                    color: "primary",
                                    disabled: _vm.kondisiVerify == false,
                                  },
                                  on: {
                                    click: function ($event) {
                                      return _vm.verifyOTPCodes((_vm.e2p = 3))
                                    },
                                  },
                                },
                                [
                                  _vm._v(
                                    "Verify\n                              "
                                  ),
                                ]
                              ),
                              _vm._v(" "),
                              _c("br"),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    block: "",
                                    color: "red",
                                    disabled: _vm.select == "admin",
                                  },
                                  on: {
                                    click: function ($event) {
                                      return _vm.getNewOTPCodes()
                                    },
                                  },
                                },
                                [
                                  _vm._v(
                                    "Request New OTP Codes\n                              "
                                  ),
                                ]
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-stepper-content",
                { attrs: { step: "3" } },
                [
                  _c(
                    "v-card",
                    [
                      _c(
                        "v-responsive",
                        { attrs: { "aspect-ratio": 4 / 3 } },
                        [
                          _c("v-card-text", { attrs: { align: "center" } }, [
                            _c("b", [
                              _vm._v(
                                "Thank Your For Your Register. Please click below button"
                              ),
                            ]),
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              staticClass: "ma-2",
                              attrs: { position: "center", color: "info" },
                              on: {
                                click: function ($event) {
                                  return _vm.succesRegister()
                                },
                              },
                            },
                            [
                              _vm._v(
                                "\n                              Continue\n                          "
                              ),
                            ]
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);