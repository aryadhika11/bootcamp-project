"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_Login_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Login.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Login.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'login',
  data: function data() {
    var _this = this;

    return {
      e1: 1,
      valid: true,
      validForgotPassword: false,
      email: '',
      verifyEmail: '',
      emailRules: [function (v) {
        return !!v || 'E-mail is required';
      }, function (v) {
        return /([a-zA-Z0-9_]{1,})(@)([a-zA-Z0-9_]{2,}).([a-zA-Z0-9_]{2,})+/.test(v) || 'E-mail must be valid';
      }],
      showPassword: false,
      showPasswordConfirm: false,
      password: '',
      passwordRules: [function (v) {
        return !!v || 'password required.';
      }, function (v) {
        return v && v.length >= 8 || 'Min 8 characters';
      }],
      confirmpasswordRules: [function (v) {
        return v == _this.newPassword || 'Your password must be same';
      }, function (v) {
        return !!v || 'password required.';
      }],
      newPassword: '',
      confirmPassword: '',
      kondisi: false,
      counter: 0
    };
  },
  computed: _objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_0__.mapGetters)({
    user: 'auth/user'
  })),
  methods: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_0__.mapActions)({
    setAlert: 'alert/set',
    setAuth: 'auth/set'
  })), {}, {
    submit: function submit() {
      var _this2 = this;

      if (this.counter <= 5) {
        if (this.$refs.form.validate()) {
          var formData = {
            'email': this.email,
            'password': this.password
          };
          axios.post('/api/auth/login', formData).then(function (response) {
            if (response.data.status_code != '01') {
              _this2.setAuth(response.data.data.users);

              _this2.setAlert({
                status: true,
                color: 'success',
                text: 'Login Success'
              }), _this2.close();
            } else {
              _this2.setAlert({
                status: true,
                color: 'error',
                text: response.data.message
              });

              _this2.kondisi = true;
              _this2.counter += 1;
            }
          })["catch"](function (error) {
            var responses = error.response;

            _this2.setAlert({
              status: true,
              text: responses.data.error,
              color: 'error'
            });

            _this2.counter += 1;
          });
        }
      } else {
        this.setAlert({
          status: true,
          text: 'Sorry, You have tried ' + this.counter + ' times to login, Please to Register!',
          color: 'error'
        });
        this.email = '';
        this.password = '';
        this.close();
        window.location.reload();
      }
    },
    verifyPassword: function verifyPassword(e1p) {
      var _this3 = this;

      if (this.newPassword != '') {
        if (this.newPassword != '' && this.confirmPassword != '') {
          var formData = {
            'email': this.verifyEmail,
            'password': this.newPassword,
            'password_verification': this.confirmPassword
          };
          axios.post('/api/auth/updatePassword', formData).then(function (response) {
            if (response.data.status_code != '01') {
              _this3.setAlert({
                status: true,
                color: 'success',
                text: response.data.message
              });
            } else {
              _this3.setAlert({
                status: true,
                text: response.data.message,
                color: 'error'
              });
            }
          })["catch"](function (error) {
            var responses = error.response;

            _this3.setAlert({
              status: true,
              text: responses.data.error,
              color: 'error'
            });
          });
        } else {
          this.setAlert({
            status: true,
            text: 'New password and confirm password is required',
            color: 'error'
          });
        }
      } else {
        if (this.verifyEmail != '') {
          var verifyEmail = {
            'email': this.verifyEmail
          };
          axios.post('/api/auth/updatePassword', verifyEmail).then(function (response) {
            if (response.data.status_code != '01') {
              _this3.e1 = e1p;
            } else {
              _this3.setAlert({
                status: true,
                text: response.data.message,
                color: 'error'
              });
            }
          })["catch"](function (error) {
            var responses = error.response;

            _this3.setAlert({
              status: true,
              text: responses.data.error,
              color: 'error'
            });
          });
        } else {
          this.setAlert({
            status: true,
            text: 'Email is required',
            color: 'error'
          });
        }
      }
    },
    close: function close() {
      this.$emit('closed', false);
    },
    authProvider: function authProvider(provider) {
      var _this4 = this;

      var url = '/api/auth/social/' + provider;
      axios.get(url).then(function (response) {
        var data = response.data;
        window.location.href = data.url;
      })["catch"](function (error) {
        _this4.setAlert({
          status: true,
          text: error.message,
          color: 'error'
        });
      });
    }
  })
});

/***/ }),

/***/ "./resources/js/components/Login.vue":
/*!*******************************************!*\
  !*** ./resources/js/components/Login.vue ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=6bdc8b8e& */ "./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/components/Login.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__.render,
  _Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Login.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Login.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/components/Login.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Login.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Login.vue?vue&type=template&id=6bdc8b8e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e& ***!
  \*****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    [
      _c(
        "v-toolbar",
        { attrs: { dark: "", color: "success" } },
        [
          _c(
            "v-btn",
            {
              attrs: { icon: "", dark: "" },
              nativeOn: {
                click: function ($event) {
                  return _vm.close.apply(null, arguments)
                },
              },
            },
            [_c("v-icon", [_vm._v("mdi-close")])],
            1
          ),
          _vm._v(" "),
          _c("v-toolbar-title", [_vm._v("Login")]),
        ],
        1
      ),
      _vm._v(" "),
      _c("v-divider"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "" } },
        [
          _c(
            "v-form",
            {
              ref: "form",
              attrs: { "lazy-validation": "" },
              model: {
                value: _vm.valid,
                callback: function ($$v) {
                  _vm.valid = $$v
                },
                expression: "valid",
              },
            },
            [
              _c("v-text-field", {
                attrs: {
                  rules: _vm.emailRules,
                  label: "E-mail",
                  required: "",
                  "append-icon": "mdi-email",
                },
                model: {
                  value: _vm.email,
                  callback: function ($$v) {
                    _vm.email = $$v
                  },
                  expression: "email",
                },
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "append-icon": _vm.showPassword ? "mdi-eye" : "mdi-eye-off",
                  rules: _vm.passwordRules,
                  type: _vm.showPassword ? "text" : "password",
                  label: "Password",
                  hint: "At least 8 characters",
                  counter: "",
                },
                on: {
                  "click:append": function ($event) {
                    _vm.showPassword = !_vm.showPassword
                  },
                },
                model: {
                  value: _vm.password,
                  callback: function ($$v) {
                    _vm.password = $$v
                  },
                  expression: "password",
                },
              }),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "text-xs-center" },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: {
                        color: "success lighten-1",
                        disabled: !_vm.valid,
                      },
                      on: { click: _vm.submit },
                    },
                    [
                      _vm._v(
                        "\n                  \n                  Login\n                  "
                      ),
                      _c("v-icon", { attrs: { right: "", dark: "" } }, [
                        _vm._v("mdi-lock-open"),
                      ]),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "primary lighten-1" },
                      on: {
                        click: function ($event) {
                          return _vm.authProvider("google")
                        },
                      },
                    },
                    [
                      _vm._v(
                        "\n                  \n                  Login with google\n                  "
                      ),
                      _c("v-icon", { attrs: { right: "", dark: "" } }, [
                        _vm._v("mdi-google"),
                      ]),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _c(
            "div",
            [
              _c(
                "v-dialog",
                {
                  attrs: { width: "500" },
                  scopedSlots: _vm._u([
                    {
                      key: "activator",
                      fn: function (ref) {
                        var on = ref.on
                        var attrs = ref.attrs
                        return [
                          _c(
                            "v-btn",
                            _vm._g(
                              _vm._b(
                                { attrs: { color: "red lighten-2", dark: "" } },
                                "v-btn",
                                attrs,
                                false
                              ),
                              on
                            ),
                            [
                              _vm._v(
                                "\n                          Forgot Password\n                          "
                              ),
                            ]
                          ),
                        ]
                      },
                    },
                  ]),
                },
                [
                  _vm._v(" "),
                  _c(
                    "v-card",
                    [
                      _c(
                        "v-card-title",
                        { staticClass: "text-h5 grey lighten-2" },
                        [
                          _vm._v(
                            "\n                          Get your new password\n                          "
                          ),
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-stepper",
                        {
                          model: {
                            value: _vm.e1,
                            callback: function ($$v) {
                              _vm.e1 = $$v
                            },
                            expression: "e1",
                          },
                        },
                        [
                          _c(
                            "v-stepper-header",
                            [
                              _c(
                                "v-stepper-step",
                                { attrs: { complete: _vm.e1 > 1, step: "1" } },
                                [
                                  _vm._v(
                                    "\n                                      Email\n                                  "
                                  ),
                                ]
                              ),
                              _vm._v(" "),
                              _c("v-divider"),
                              _vm._v(" "),
                              _c(
                                "v-stepper-step",
                                { attrs: { complete: _vm.e1 > 2, step: "2" } },
                                [
                                  _vm._v(
                                    "\n                                      New password\n                                  "
                                  ),
                                ]
                              ),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-stepper-items",
                            [
                              _c(
                                "v-stepper-content",
                                { attrs: { step: "1" } },
                                [
                                  _c(
                                    "v-form",
                                    {
                                      ref: "form",
                                      attrs: { "lazy-validation": "" },
                                      model: {
                                        value: _vm.validForgotPassword,
                                        callback: function ($$v) {
                                          _vm.validForgotPassword = $$v
                                        },
                                        expression: "validForgotPassword",
                                      },
                                    },
                                    [
                                      _c("v-text", [
                                        _vm._v(
                                          "\n                                      We need to know if you have an account to change your password.\n                                      "
                                        ),
                                      ]),
                                      _vm._v(" "),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c("v-text-field", {
                                        attrs: {
                                          rules: _vm.emailRules,
                                          label: "E-mail",
                                          required: "",
                                          "append-icon": "mdi-email",
                                        },
                                        model: {
                                          value: _vm.verifyEmail,
                                          callback: function ($$v) {
                                            _vm.verifyEmail = $$v
                                          },
                                          expression: "verifyEmail",
                                        },
                                      }),
                                      _vm._v(" "),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: {
                                            disabled: !_vm.validForgotPassword,
                                            color: "primary",
                                          },
                                          on: {
                                            click: function ($event) {
                                              return _vm.verifyPassword(
                                                (_vm.e1p = 2)
                                              )
                                            },
                                          },
                                        },
                                        [
                                          _vm._v(
                                            "\n                                          Continue\n                                      "
                                          ),
                                        ]
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-stepper-content",
                                { attrs: { step: "2" } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      "append-icon": _vm.showPassword
                                        ? "mdi-eye"
                                        : "mdi-eye-off",
                                      rules: _vm.passwordRules,
                                      type: _vm.showPassword
                                        ? "text"
                                        : "password",
                                      label: "New Password",
                                      hint: "At least 8 characters",
                                      counter: "",
                                    },
                                    on: {
                                      "click:append": function ($event) {
                                        _vm.showPassword = !_vm.showPassword
                                      },
                                    },
                                    model: {
                                      value: _vm.newPassword,
                                      callback: function ($$v) {
                                        _vm.newPassword = $$v
                                      },
                                      expression: "newPassword",
                                    },
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      "append-icon": _vm.showPasswordConfirm
                                        ? "mdi-eye"
                                        : "mdi-eye-off",
                                      rules: _vm.confirmpasswordRules,
                                      type: _vm.showPasswordConfirm
                                        ? "text"
                                        : "password",
                                      label: "Confirm New Password",
                                      counter: "",
                                    },
                                    on: {
                                      "click:append": function ($event) {
                                        _vm.showPasswordConfirm =
                                          !_vm.showPasswordConfirm
                                      },
                                    },
                                    model: {
                                      value: _vm.confirmPassword,
                                      callback: function ($$v) {
                                        _vm.confirmPassword = $$v
                                      },
                                      expression: "confirmPassword",
                                    },
                                  }),
                                  _vm._v(" "),
                                  _c("br"),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: { color: "primary" },
                                      on: {
                                        click: function ($event) {
                                          return _vm.verifyPassword()
                                        },
                                      },
                                    },
                                    [
                                      _vm._v(
                                        "\n                                  Continue\n                                  "
                                      ),
                                    ]
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);