<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function random($count)
    {
        $blog = Blog::select('*')->inRandomOrder()->limit($count)->get();

        $data['blogs'] = $blog;

        return response()->json([
            'response code' => '00',
            'response message' => 'Data Blog Berhasil Ditampilkan',
            'data' => $data
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg'
        ]);

        $campaign = Blog::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $campaign->id . "." . $image_extension;
            $image_folder = '/photos/blog/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);
                
                $campaign->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Photo Profile Gagal Upload',
                ], 201);
            }

            $data['campaign'] = $campaign;

            return response()->json([
                'response_code' => '00',
                'response_message' => 'Data blog Berhasil Ditambahkan',
                'data' => $data
            ], 200);
        }
    }
}
