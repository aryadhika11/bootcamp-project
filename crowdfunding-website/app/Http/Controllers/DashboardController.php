<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getProfile()
    {

        $ambilUser = auth()->user();

        return response()->json([
            'status code' => '00',
            'message' => 'Anda Berhasil Login',
            'data' => [
                'users' =>  $ambilUser
            ]
        ], 200);
    }

    public function updateProfile(Request $request)
    {
        $ambilUser = auth()->user();
        $ubahProfile = User::where('id', $ambilUser->id)->update([
            'username' => $request->username,
            'name' => $request->name
        ]);

        $cekUser = User::where('id', $ambilUser->id)->first();
        
        return response()->json([
            'status code' => '00',
            'message' => 'Profile anda berhasil diperbaharui',
            'data' => [
                'users' => $cekUser
            ]
        ], 200);

    }
}
