<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campaign;

class CampaignController extends Controller
{
    public function random($count)
    {
        $campaigns = Campaign::select('*')->inRandomOrder()->limit($count)->get();

        $data['campaigns'] = $campaigns;

        return response()->json([
            'response code' => '00',
            'response message' => 'Data Campaign Berhasil Ditampilkan',
            'data' => $data
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
            'address' => 'required|string',
            'required' => 'required',
            'collected' => 'required'
        ]);

        $campaign = Campaign::create([
            'title' => $request->title,
            'description' => $request->description,
            'address' => $request->address,
            'required' => $request->required,
            'collected' => $request->collected
        ]);

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $campaign->id . "." . $image_extension;
            $image_folder = '/photos/campaign/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $campaign->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Photo Profile Gagal Upload',
                ], 201);
            }

            $data['campaign'] = $campaign;

            return response()->json([
                'response_code' => '00',
                'response_message' => 'Data Campaigns Berhasil Ditambahkan',
                'data' => $data
            ], 200);
        }
    }
    public function index()
    {
        $campaigns = Campaign::paginate(6);

        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Data Campaigns Berhasil Ditampilkan',
            'data' => $data
        ], 200);
    }

    public function detail($id)
    {

        $campaign = Campaign::find($id);

        $data['campaign'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Data Campaigns Berhasil Ditampilkan',
            'data' => $data
        ], 200);
    }

    public function search($keyword) {
        $campaigns = Campaign::select('*')->where('title', 'LIKE', "%".$keyword."%")->get();

        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00', 
            'response_message' => 'Data Campaigns Berhasil Ditampilkan',
            'data' => $data
        ], 200);
    }
    
    public function delete($id){
        $del = Campaign::find($id)->delete();

        if($del) {
            return response()->json([
                'response_code' => '00', 
                'response_message' => 'Data Campaigns Berhasil Dihapus',
            ], 200);
        }else{
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Data Campaigns Gagal Dihapus',
            ], 201);
        }
    }

    public function edit(Request $request, $id) {
        return $request;
        $edit = Campaign::find($id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'address' => $request->address,
            'required' => $request->required,
            'collected' => $request->collected
        ]);

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $edit->id . "." . $image_extension;
            $image_folder = '/photos/campaign/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $edit->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Photo Profile Gagal Upload',
                ], 201);
            }

            $data['campaign'] = $edit;

            return response()->json([
                'response_code' => '00',
                'response_message' => 'Data Campaigns Berhasil Diupdate',
                'data' => $data
            ], 200);
        }

    }
}
