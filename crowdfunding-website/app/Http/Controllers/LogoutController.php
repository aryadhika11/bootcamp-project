<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogoutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        
        auth()->logout();
        
        return response()->json([
            'response_code' => '00',
            'response_message' => "User berhasil logout",
        ],200);
    }
}
