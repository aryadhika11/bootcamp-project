<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Carbon\Carbon;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider)
    {

        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider)
    {
        try {
            $social_user = Socialite::driver($provider)->stateless()->user();

            if (!$social_user) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'login failed'
                ], 401);
            }

            $user = User::whereEmail($social_user->email)->first();

            if (!$user) {
                if ($provider == 'google') {
                    $image_avatar = $social_user->avatar;
                }
                $user = User::create([
                    'email' => $social_user->email,
                    'username' => $social_user->name,
                    'name' => $social_user->name,
                    'email_verified_at' => Carbon::now(),
                    'image' => $image_avatar,
                    'role_id' => '83822223-f525-4b53-92a8-ec90118db214'
                ]);
            }

            $data['user'] = $user;
            $data['token'] = auth()->login($user);

            return response()->json([
                'response_code' => '00',
                'response_message' => 'login berhasil',
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'response_code' => '01',
                'response_message' => $th->getMessage()
            ], 401);
        }
    }
    
    public function getNewPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|min:8'
        ], [
            'password.required' => 'password anda wajib diisikan',
            'password.min' => 'password anda minimal 8 karakter'
        ]);

        $ambilUser = User::where('email', $request->email)->update([
                'password' => md5($request->password)
        ]);

        if ($ambilUser) {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'Password berhasil ditambahkan',
            ], 200);
        } else {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Password anda gagal ditambahkan'
            ], 401);
        }
    }
}
