<?php

namespace App\Http\Controllers;

use App\Events\UserGenerateCode;
use App\Events\UserRegisteredEvent;
use App\Models\OtpCodes;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LoginSignupController extends Controller
{

    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required|string|min:1',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|min:8|max:20',
            'name' => 'required|string',
            'image' => 'required|mimes:png,jpg,jpeg',
        ], [
            'username.string' => 'username harus berbentuk kalimat atau string',
            'email.unique' => 'email sudah digunakan',
            'password.min' => 'password minimal 8 karakter',
            'password.max' => 'password maximal 8 karakter',
            'name.string' => 'nama anda harus berbentuk kalimat atau string',
            'image.mimes' => 'Image must be in the format .png, .jpg, .jpeg'
        ]);
        if ($request->role_id === 'admin') {
            $role = Role::where('name', $request->role_id)->first();
            $ambilRoleId = $role->id;
        } else if ($request->role_id === 'user') {
            $role = Role::where('name', $request->role_id)->first();
            $ambilRoleId = $role->id;
        }

        $buatAkun = User::Create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => md5($request->password),
            'name' => $request->name,
            'role_id' => $ambilRoleId
        ]);

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $buatAkun->id . "." . $image_extension;
            $image_folder = '/photos/user/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $buatAkun->update([
                    'image' => $image_location
                ]);

            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Photo Profile Gagal Upload',
                ], 201);
            }

            $ambilID = User::where('email', $request->email)->first();
            OtpCodes::create([
                'otp' => mt_rand(100000, 999999),
                'user_id' => $ambilID->id,
                'valid_until' => Carbon::now()->addMinutes(5)
            ]);

            $getOTP = OtpCodes::where('user_id', $ambilID->id)->first();

            $tampung = [
                'username' => $ambilID->username,
                'email' => $ambilID->email,
                'otp' => $getOTP->otp
            ];

            event(new UserRegisteredEvent($tampung));

            if ($buatAkun) {
                return response()->json([
                    'status code' => '00',
                    'message' => 'Cek Email Segera',
                    'data' => [
                        'users' => $buatAkun,
                    ],
                ], 200);
            }
            return response()->json([
                'status code' => '01',
                'message' => 'Anda Gagal Masuk',
            ], 201);
        }
    }

    public function verifyOTPCodes(Request $request)
    {

        $ambilOTP = OtpCodes::where('otp', $request->otp)->first();

        if (!$ambilOTP) {
            return response()->json([
                'status_code' => '01',
                'message' => 'Kode OTP anda tidak ditemukan',
            ], 201);
        }

        $waktuSekarang = Carbon::now();

        if ($waktuSekarang > $ambilOTP->valid_until) {
            $this->simpanOTP = $ambilOTP->otp;
            return response()->json([
                'status_code' => '01',
                'message' => 'Kode OTP anda sudah tidak berlaku. Segera request OTP kembali!',
            ], 201);
        } else {

            User::where('id', $ambilOTP->user_id)->update([
                'email_verified_at' => Carbon::now()
            ]); 

            $ambilDataUser = User::where('id', $ambilOTP->user_id)->first();
            return response()->json([
                'status_code' => '00',
                'message' => 'Kode OTP anda berhasil',
                'data' => [
                    'user' => $ambilDataUser
                ]
            ], 200);
        }
    }

    public function regenerateOTPCodes(Request $request)
    {
        $cariEMail = User::where('email', $request->email)->first();

        OtpCodes::where('user_id', $cariEMail->id)->update([
            'otp' => mt_rand(100000, 999999),
            'valid_until' => Carbon::now()->addMinutes(5)
        ]);

        $cekOTP = OtpCodes::where('user_id', $cariEMail->id)->first();

        $tampung = [
            'email' => $cariEMail->email,
            'otp' => $cekOTP->otp
        ];

        event(new UserGenerateCode($tampung));

        return response()->json([
            'status code' => '00',
            'message' => 'Cek email anda segera',
            'data' => [
                'email' => $cariEMail->email
            ]
        ], 200);
    }

    public function updatePassword(Request $request)
    {

        $ambilUser = User::where('email', $request->email)->first();

        if ($ambilUser) {
            if ($request->password === $request->password_verification) {
                User::where('email', $request->email)->update([
                    'password' => md5($request->password),
                ]);

                return response()->json([
                    'status_code' => '00',
                    'message' => 'Update Password Berhasil!',
                    'data' => [
                        'users' => $ambilUser
                    ]
                ], 200);
            } else {
                return response()->json([
                    'status_code' => '01',
                    'message' => 'Password anda tidak sama',
                ], 201);
            }
        } else {
            return response()->json([
                'status_code' => '01',
                'message' => 'email anda tidak ditemukan',
            ], 201);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|min:6|max:20',
        ], [
            'email.required' => 'email harus diisikan',
            'password.required' => 'password harus diisikan',
            'password.min' => 'password minimal 6 karakter',
            'password.max' => 'password maximal 20 karakter'
        ]);

        $ambilEmail = User::where('email', $request->email)->first();

        if ($ambilEmail) {
            if ($ambilEmail->password === md5($request->password)) {
                if ($ambilEmail->email_verified_at !== null) {
                    $user = \App\Models\User::where([
                        'email' => $request->email,
                        'password' => md5($request->password)
                    ])->first();

                    if (!$token = auth()->guard('api')->login($user)) {
                        return response()->json([
                            'status_code' => '01',
                            'message' => 'Email anda tidak valid',
                        ], 201);
                    } else {
                        return response()->json([
                            'status_code' => '00',
                            'message' => 'Anda Berhasil Login',
                            'data' => [
                                'users' => [
                                    'token' => $token,
                                    'user' => $ambilEmail
                                ]
                            ]
                        ], 200);
                    }
                }
                return response()->json([
                    'status_code' => '01',
                    'message' => 'Email anda belum diverfikasi',
                ], 201);
            }
            return response()->json([
                'status_code' => '01',
                'message' => 'Password anda tidak sama',
            ], 201);
        }
        return response()->json([
            'status_code' => '01',
            'message' => 'Email anda tidak cocok',
        ], 201);
    }
}
