<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckTokenController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return response()->json([
            'response_code' => '00',
            'response_message' => 'token valid',
            'data' => true
        ], 200);
    }
}
