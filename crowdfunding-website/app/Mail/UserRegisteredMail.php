<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    public $username;
    public $email;
    public $otp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->username = $user->username;
        $this->email = $user->email;
        $this->otp = $user->otp;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('crowdfunding@gmail.com')
                ->view('MailSender.mail')
                ->with([
                    'username' => $this->username,
                    'otp' => $this->otp
                ]);
    }
}
