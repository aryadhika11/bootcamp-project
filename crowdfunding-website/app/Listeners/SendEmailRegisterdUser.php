<?php

namespace App\Listeners;

use App\Events\UserGenerateCode;
use App\Events\UserRegisteredEvent;
use App\Mail\UserGenerateCode as MailUserGenerateCode;
use App\Mail\UserRegisteredMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailRegisterdUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    //handle to User registered from event
    public function handleUserRegistered(UserRegisteredEvent $user)
    {
        Mail::to($user->email)->send(new UserRegisteredMail($user));
    }

    //handle to user regenerate code from event
    public function handleUserCode(UserGenerateCode $tampung)
    {
        Mail::to($tampung->email)->send(new MailUserGenerateCode($tampung));
    }

    public function subscribe()
    {
        return [
            UserRegisteredEvent::class => 'handleUserRegistered',
            UserGenerateCode::class => 'handleUserCode',
        ];
    }
}
