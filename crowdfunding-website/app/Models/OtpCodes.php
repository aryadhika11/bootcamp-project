<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class OtpCodes extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = [
        'otp',
        'user_id',
        'valid_until'
    ];


    protected static function boot () {
        parent::boot();

        static::creating( function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName() } = Str::uuid();
            }
        });
    }
    
    public function user () {
        return $this->hasOne(User::class);
    }
}
