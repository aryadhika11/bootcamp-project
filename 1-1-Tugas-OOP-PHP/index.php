<?php

abstract class hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    abstract public function atraksi();
}

trait fight
{
    public $attackPower;
    public $defencePower;

    public function serang($Diserang)
    {
        echo $this->nama. " sedang menyerang " .$Diserang->nama;
        $this->diserang($Diserang);
    }

    public function diserang($Diserang)
    {
        echo " . ";
        echo $Diserang->nama. " sedang diserang";
        $Diserang->darah =  $Diserang->darah - $this->attackPower / $Diserang->defencePower;
        echo " . ";
        echo "Darah yang tersisa ".$Diserang->nama. " adalah " .$Diserang->darah;
    }
}

class elang extends hewan
{
    use fight;
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "Terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function atraksi()
    {
        echo $this->nama. " sedang " .$this->keahlian;
    }

    public function getInfoHewan () 
    {
        echo "
        Nama : ".$this->nama. "
        Darah : " . $this->darah."
        Jumlah kaki : " . $this->jumlahKaki. "
        Keahlian : " . $this->keahlian."
        Jenis : Elang 
        Attack Power : " . $this->attackPower. "
        Defence Power : " . $this->defencePower;

    }
}
class harimau extends hewan
{
    use fight;

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "Lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function atraksi()
    {
        echo $this->nama. " sedang " .$this->keahlian;
    }

    public function getInfoHewan () 
    {
        echo "
        Nama : ".$this->nama. "
        Darah : " . $this->darah."
        Jumlah kaki : " . $this->jumlahKaki. "
        Keahlian : " . $this->keahlian."
        Jenis : Harimau 
        Attack Power: " . $this->attackPower. "
        Defence Power : " . $this->defencePower;

    }
}

//pemanggilan kelas elang dan harimau
$obj1 = new elang("elang_3");
$obj2 = new harimau("harimau_1");

//pemanggilan object class
// echo $obj1->atraksi();
// echo $obj1->serang($obj2);
// echo $obj2->serang($obj1);
// echo $obj1->getInfoHewan();
echo $obj2->getInfoHewan();

